= The Stable Maintainer Role =

== Goals ==

* Have stable maintainers
* Work out what their role is
* Document the master maintainer and stable maintainer roles
* Make sure Nick doesn't have to do stable maintainer all the time

== Current Process ==

Initial merge:
1. Find merge_ready tickets
2. Look at it all (each ticket? the list of tickets?)
3. Check CI
  * Can we automate checking CI?
4. Check the ticket and branch targets
  * Can we automate checking the branch target?
5. If the target is master, do a merge to master.
6. If the target is an earlier release, and it is a trivial fix, do a merge to the target.
7. If it is a non-trivial backport, do a merge to master, and mark for backport.

Backport merge:
1. Work out how severe it is, how risky it is, and how tested it is.
2. If we decide to backport to a release, do a merge to that release.

Doing a merge:
1. Read the code.
2. Merge the branch.
3. Merge forward if needed.
4. If it is not marked for backport, close the ticket, and the pull request.
  * GitHub closes pull requests, unless we have changed the commits before merge. How can we automate manual closes?

Doing a release:
1. If we have an active alpha series, put out an alpha every few weeks.
2. Put out a stable release every 1-2 months.
3. Put out oldstable and LTS releases as needed.

Evaluating backports:
1. Ideal backports are:
  * high-severity,
  * low-risk,
  * low-complexity,
  * tested in multiple alphas, including Tor Browser and Debian.
2. The most difficult backports are security fixes:
  * high-severity,
  * high-risk,
  * untested.

== Proposals ==

1. Always merge to master first (no trivial backports)
2. Avoid last-minute, high-risk backports. Always backport to the branch a few weeks before the release.
3. Always backport to version x, and all later versions. If needed, backport to version y later (y < x).
  * Make consistent decisions
4. Have a 3-person rule: coder, reviewer, and master-merger must be different people.
  * Stable maintainers can be any one of the 3.
5. Always check CI on branches pre-merge, and pre-push to git.torproject.org
   * Unless the commits are exactly the same in pre-merge and pre-push
6. Proposed merger roles:
|| Release    || Timing                   || nickm     || dgoulet    || asn            || teor    ||
|| master     || at least once per week   || primary   || HS, Kist   || HS, Guards, PT || ?       ||
|| backports  || about once every 2 weeks || secondary || (as above) || (as above)     || primary ||

The list of subsystems that dgoulet and asn maintain are in:
https://gitweb.torproject.org/tor.git/tree/doc/HACKING/Maintaining.md#n44

== Action Items ==

0. Open tickets for action items
1. Update stable maintainer guidelines
2. Put a maintainer table on the network team wiki
3. Update commit bits in gitolite
4. Work out if we can disable merging to unsupported branches
5. Work out if we can disable accidentally pushing feature branches
6. Work out if we can stop back-merging (merging a branch based on master to an earlier maint branch)
  * Using a script?
7. Discuss with team: do we want regular stable release dates?
8. Update merge role subsystems from subsystems maintainer document? Or just reference that document?
