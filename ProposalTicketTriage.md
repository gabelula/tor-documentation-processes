= Pad created on December 4th - Expires in 60 days from last time used=

Network team triage and proposed tickets


Proposal: Let's do Proposed Tickets better

We need to stop adding lots of tickets to every release.


Here's one way to do that:

Any time:
* new tickets go in Tor: unspecified
* tag proposed tickets with 041-proposed, [and give them points? -nm] done!
* assign points
* assign a sponsor, if possible

Before the meeting:
* find all the tickets which have been added to the 0.4.1 milestone, but are not 041-approved
* add these new tickets to 041-proposed
  * they will be a mix of urgent tickets, subtickets, volunteer tickets, and random tickets
* check each proposed ticket:
  * does the ticket have a points estimate? [I propose that adding said estimate be part of proposing the ticket, if possible. -nm] done!
* work out our current and proposed capacity:
  * do we have enough time left in 0.4.1 to do all 041-approved tickets?
  * how many 041-proposed tickets can we add?
  * do we need to remove any tickets from 041-approved?
    * tag them with 041-proposed-remove

At the meeting:
* look at our current and proposed capacity
* check each proposed ticket:
  * do we want/need to do this task in 0.4.1? (Roadmap/Sponsor/Urgent)
  * do we have time to do this task in 0.4.1? (Points/Capacity)

After the meeting:
* to add proposed tickets:
    * move them into the 0.4.1 milestone
    * tag them with 041-approved-(date)
    * remove 041-proposed*
* to remove tickets:
    * move them into Tor: unspecified
    * tag them with 041-unreached-(date)
    * remove 041-approved* and 041-proposed*
* update the roadmap
* work out our current capacity for 0.4.1


Ideal state:

All 0.4.1 tickets have a points estimate.
The 0.4.1 roadmap is under capacity.

All tickets in the 0.4.1 milestone are:
* on the roadmap
* 041-approved
* not 041-unreached

All tickest in Core Tor/Tor are:
* not 041-proposed


QUESTIONS:
    * What does the triage person do for milestone assignment with proposed tickets?
      teor says: the bug triage person puts all tickets in "Tor: unspecified"
                 if the team agrees to put a proposed ticket in a milestone, the ticket goes in that milestone

Nick has some questions:
    * What do we do with tickets that are regression bugs, trivial, etc?
      teor says: let's try putting all our tickets through this process.
                 Trivial tickets can get 0.1 points.
                 Small tickets, roadmap tickets, and critical tickets should be checked for points and sponsor,
                 then approved without discussion.
    * Do we use the pad to accumulate tickets for discussion too?
      teor says: let's use the pad for tickets that need discussion: large tickets that aren't on the roadmap
    * What do we do with tickets that are currently in 0.4.0 and earlier?
      teor says: let's try to keep 0.4.1 contained
                 We can dump old tickets in Tor: unspecified, then put roadmap tickets in 041-proposed
    * What do we do with bugs that people find in current releases?
      teor says: Usually, bugs in current releases are also in master, so we can use this process?

teor has some questions:
    * What if our estimates are really low?
      Suggestion: this is out of scope for the moment, let's think about it in a month or two
      Suggestion: work out our points velocity every month, and use that velocity to work out how many points we can do before feature freeze
      Suggestion: compare estimated and actual points, and multiply future estimates by the ratio



Existing Processes that Work

Here's what we do right now, and we should keep doing:
* each release is roadmapped at our team meetings
* we put tickets in the release if they're on the roadmap
* changes to the roadmap get discussed at weekly meetings, and then we update the tickets in the release
* bug fixes and features with code go in the latest milestone
  * paid staff can do quick fixes and features up to half a day per week without changing the roadmap
  * volunteers get their contributions reviewed

Problems with Existing Processes

What are the Tor milestones in trac used for?
* tickets that are on the roadmap
* tickets that we all agree are urgent (CI, regressions)
* code from volunteers
* things someone would like to do in the release, or would like to put on the roadmap

Heres what I want to change:
* Sometimes, people just add tickets to the release
* We currently tag tickets with 040-roadmap-proposed, but that doesn't work, because we don't talk about specific tickets at the meeting.
