{{{

Goal: proposed guidelines for team on reviews



Question: What things are dissatisfied about wrt the review process



Desires:
  * More automation
  * more tools to check for things we miss
  * enumeration of things we miss
  * less backlog
       * (but also it's hard to do stuff fast)
  * tooling inconsistency
  * faster tunaround on volunteer reviews
      * minimal typical expectation for volunteer reviews


CI ideas:
  * chutney
  * changes files
  * coverage determinism


Suggestions:
  * okay to delay low-pri reviews
  * review points
  * technical debt has happened because of current review process; need to
    review in a way to avoid technical debt.
  * Action item: CI is deterministic
  * Action item: two reviewers for big patches.
  * Action item: it's okay to ask for help with reviews.  It's better to
    ask for help than for to backlog.


What are we saying when we say merge ready:
  * "lgtm"

  * level of confidence

  * how nice is the code

  * Action item: When you say merge_ready, explain what you think about the
    code: use your words.

  * Action item: when you take something _out_ of merge ready, explain
    why and use a marker of some time.


Potential minimal requirements; potential extras.

  * Tests pass or CI failure explained/annotated with ticket

  * Has changes file if needed

  * Read & understood the code

  * Note any extra non-CI tests you run

  * Note any configuration tests not handled by CI
       * Run it or open a ticket for it

  * Allowable Coverage reduction? (Action item: lower our threshold.)

  * What problems should you note:
       * Commit structure not readable
           * unrelated changes in 1 commit
           * commits should tell a story
           * separate movement, refactoring, and behavior changes
           * separate bugs and features
       * Bugs
       * Duplicated code
       * Maintenance problems in the future
       * Conformance to best practices and coding standards
           * Large new features in new modules


       * Confidence level: How sure do you need to be?
           * Very sure about stable; pretty sure about master?
           * Very sure about security patches
           * Very sure that there aren't new vulnerabilities introduced by
             patch.

Action item:
  * best practices for structuring BIIIG feature branch
     * best practices for large branches of disabled code?

}}}



add policy about leave
  - no reviews for people on leave
  - if you go on leave and want to keep a review you hae to mention so
