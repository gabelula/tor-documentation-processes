# Role: Engineer

## What to work on next?

1. High security Bugs
  . from secret mailing list
  . https://trac.torproject.org/projects/tor/query?priority=%5EImmediate&status=accepted&status=assigned&status=merge_ready&status=needs_information&status=needs_review&status=needs_revision&status=new&status=reopened&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=component&order=priority

2. Reviews for the week

Here are the needs-review tickets, by reviewer:
    https://trac.torproject.org/projects/tor/query?status=needs_review&reviewer=!&max=300&col=id&col=summary&col=reviewer&col=status&col=type&col=priority&col=milestone&col=component&order=reviewer&report=82

Here are the outstanding reviews, oldest first:
    https://trac.torproject.org/projects/tor/query?status=needs_review&component=Core+Tor%2FTor&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=changetime&col=reviewer&col=keywords&order=changetime

Including sbws:
    https://trac.torproject.org/projects/tor/query?status=needs_review&component=Core+Tor%2FTor&or&status=needs_review&component=Core+Tor%2Fsbws&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=changetime&col=reviewer&col=keywords&order=changetime

3. Role for the week.

4. Roadmap tickets
  . in progress and then backlog by priority
