Source: https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/ReleaseGuidelines

# Set of guidelines for releasing Core Tor

The  goal of this guidelines is to ensure that all members of the team have  the same expectations. Release process can be super stressful for the  team, and by having guidelines we can remove a lot of stress by creating  consistency and structure to our release process.

[Release schedule](https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/CoreTorReleases#Calendar).

## Team Routines

### Each month

We plan our work in a monthly basis using the [network team's roadmap](https://storm.torproject.org/shared/nNhTJhoWUB8lSnW3hMbS42BRf2ArVCNvyeCX3zBYTzN).

#### First week of the month

We should elect the tickets we will work in that month. These should be tickets from the release milestone. During the election process we should make sure that tickets has:

 * an owner
 * a reviewer
 * estimation points (small, medium, large)

#### Last week of the month

We should review the state of the tasks of the month that is ending and:

 * look at closed tickets from previous month and apply actual points to it
 * start building an alpha release for the month (?????)
 * move open, or in review tickets to the next month tag

### Each week

We give status updates at our Monday's meetings.

We check the current 'review-group' and:

 * Remind tickets owners to check tickets in 'revision' state
 * Ask for possible reviewers to tickets that are still without a 'reviewer' assigned to it
 * Validate tickets that are 'merge-ready' and can be removed from the group

We check our [team rotation calendar](https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/TeamRotations) for who is in charge of 'bug triage' for that week.

We elect discussion topics for latter on in the meeting. These topics can be about a design implementation, or continuation of a discussion related to a proposal, or discuss the state of our releases, team processes and so on.

## Prioritization

Each release we will decide on what should guide us while deciding how to prioritize our work. This is done during ticket triage for the release - triages are done as we prepare for the release but also at any other moment we consider necessary in order to meet the release schedule.

We should always prioritize sponsor related work accordingly with the proposal timeframe. Bellow is a list of sponsors the Network Team has deliverables they are responsible for (each link takes you to our execution plan/deliverables roadmap):

 * [Sponsor 31](https://trac.torproject.org/projects/tor/wiki/org/sponsors/Sponsor31)
 * [Sponsor 19](https://trac.torproject.org/projects/tor/wiki/org/sponsors/Sponsor19)
 * [Sponsor V](https://trac.torproject.org/projects/tor/wiki/org/sponsors/SponsorV)
 * [Sponsor 2](https://trac.torproject.org/projects/tor/wiki/org/sponsors/Sponsor2)

You can use the sponsor code to query tickets related to their deliverables on Trac.

Then we should look at non-sponsor work and prioritize it accordingly with the team capacity.

(IMPORTANT BUGS OR SECURITY ISSUES TICKETS WILL GAIN PRIORITY OVER THINGS DEPENDING ON THEIR URGENCY)

## Team Capacity

How much the team can actually do.

*Out of 5 days in a week how many do we actually code?*

Based on our Developer Roles and Responsibilities document a developer has more then 'writing code' responsibility. Therefore, when we do our capacity calculation we leave off 2 days of the week for other work.

With that in mind, this is how we calculate the team capacity:

How many points a person should have:

 * 1 person per week == ~3 points
 * 1 person per month == ~12 points
 * 1 person per release == ~72 points

Points Conversion:

 * very small == 0.1 point == 1hr
 * small == 1 point == 1 day
 * small-medium == 2 points == 2 days
 * medium == 3 points == 3 days
 * medium-large == 4.5 points == 4.5 days
 * large == 6 points == 6 days

How to count for tasks that takes less than one day:

 * 1 day = 8 hrs
 * 1 hr = 0.1 point
 * 5hrs = 0.5 points


## Open questions

*Should we be working on stuff for the next release while one release is in -rc?*

  * ''Nick suggests that everybody assume they will be on bugfix during -rc time.''

*Should  we be working on stuff for the next release while there are leftover  tickets from the last one in needs_review/needs_revision?*

  * ''Nick thinks that reviewing and fixing leftover tickets should get done early, and should get some priority.''
