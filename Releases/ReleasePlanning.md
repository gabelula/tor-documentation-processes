# Release Planning v1

## Goals

* Not have too much stuff left over at the end of a release
* Plan more accurately
* Do higher-priority things first
* Make the process easy to understand and implement

* What should we do with 0.3.5, 0.4.0, and 0.4.1 milestones?

### Teor's Goals

* I want clear priorities
* I want a very clear indication of which task is next
* I want to know how much time I should spend on a task
* I want limits on the amount of tasks I can have in progress
* I want to know when I should stop and ask for help
* I want to feel like I am making progress every day
* I want to spend less time on tasks that drag and I don't feel like I am making progress
  * I want to know what I should do when a task blows up into something much larger
  * I don't know if I am expected to read all the IRC backlog
  * I often hate how much time I spend on email
  * I sometimes hate how much time I spend on various administrative tasks
* I want meetings to be very short and focused on the essential things that we all need to do

## Discussion

Roadmaps tell us which tickets belong in which releases.

### What are we bad at?

* Capacity Estimation
* Task Estimation
* Roadmapping
* Post-Roadmap Extra Tickets
* We don't have a concept of a full release
  * Points in a release, and points left over
  * Bugfix points: an estimate for unplanned fixes and their size

### What could we possibly do?

* Proposal: marking things for a milestone is good
* Proposal: putting tickets in a milestone after a capacity check is a good idea
* Proposal: one person checks capacity, and we should try to automate that check
* Proposal: when a milestone or sponsor is full, we stop adding tickets
* Proposal: regressions and security tickets go into the milestone
* Proposal: put really important must-do tickets in the release milestone
* Proposal: we allocate time for unspecified bugfixes and urgent things, and we reduce that time when we add a ticket
  * make a ticket with that time, and make it smaller when we spend the time
  * create a few buckets of time:
    * routine work
    * urgent work
    * technical debt reduction
* Proposal: tag nice-to-have tickets and put them in unspecified
* Proposal: track story points and velocity, rather than days of time
  * reference stories, to anchor estimates
* Proposal: order tasks by priority, and do the high-priority tasks first, with a time limit

### What should we do in the next few weeks?

* We need to deal with 0.3.5 and 0.4.0
  * Proposal: dump all 0.3.5 feature tickets in unspecified
  * Proposal: dump all 0.4.0 feature tickets in unspecified
  * Proposal: triage 0.3.5 and 0.4.0 bugs
* We need to create 0.4.1
  * Make the roadmap good
    * Proposal: under-promise and over-deliver
    * Proposal: work out our staff capacity for 0.4.1 in each month:
      * 238 person-coding days in 0.4.1
      * 72 person-coding days for February
      * 72 person-coding days for March
      * 63 person-coding days for April
      * 31 person-coding days for May
    * Proposal: work out the sponsor deadlines, and put sponsored work before the deadline
      * sbws finishes at the end of March, and has 18 person-coding days
      * Sponsor V finishes in September, and has 24 person-coding days
      * Sponsor 19 finishes in May, and has ??? person-coding days
      * Sponsor 31 finishes in November, and has 110 person-coding days
      * Sponsor 2 finishes in August, and has 42 person-coding days
  * Proposal: remove everything from 0.4.1 that isn't on the roadmap
  * Proposal: add tickets to 0.4.1 based on the roadmap
  * Proposal: add time to 0.4.1 for unexpected bugfixes etc.



# Release Schedule

Hi team,

In Brussels, we talked about our release management, merge policy, and
the stable maintainer role.

At the moment, here's how we do releases:
• If we have an active alpha series, put out an alpha every few weeks.
• Put out a stable release every 1-2 months.
• Put out oldstable and LTS releases as needed.

https://trac.torproject.org/projects/tor/wiki/org/meetings/2019BrusselsNetworkTeam/Notes/StableMaintainer#CurrentProcess

Do we want to schedule our stable releases on set dates?

Here's one possible schedule:
* every 8 weeks, on a Wednesday, do stable, oldstable, and LTS releases

I chose Wednesday because it's the middle of the week.
Friday failures can lead to weekend work, and my Mondays overlap with some
people's weekends.

Suggested policies:
* we can skip a release if the changes are trivial (or there are no changes)
* we can do a release early for urgent fixes. After an urgent fix, we
  reschedule future releases every 8 weeks from that date
* we can reschedule releases for leave

T

-----------------------------------------------------------------------------------


# Release planning v2


In summary:
* once we have done the roadmap, we only add essential and very small tickets
* if we add large, essential tickets, we remove other tickets
* if we do not do all our tickets by the end, we remove tickets

https://pad.riseup.net/p/network-team-triage-2018

We can talk about it at the meeting, or on the list.


= Pad created on December 4th - Expires in 60 days from last time used=

Network team triage and proposed tickets

February 2019 Version

Trial: End of February and March 2019?

Goals

    * Make roadmapped tasks achievable in the month they are scheduled
    * Make trac release milestones achievable by the end of the release
    * Make the entire roadmap achievable
    * Get closer to these goals each month, each release, and each roadmap


Process Overview

    * Plan a roadmap and release milestones
    * Don't add new tickets to the roadmap and milestones, unless they are essential or very small
    * If we run out of tickets, add one more ticket
    * If we run out of time, remove tickets/points so future months, milestones, and roadmaps are smaller
        * Ignore all the reasons that future tasks will be faster


Detailed Process

Roadmapping

Roadmap: (everyone)
    * Plan a roadmap every meeting with:
        * essential tasks
            * check for essential tasks that are already in the milestones
        * sponsored tasks
        * non-sponsored tasks
            * check if we have time and budget for non-sponsored tasks
    * Add a points estimate to each task
    * Split the roadmap into months

Release milestones: (project manager)
    * Put the roadmap tasks in each month into the milestone that covers that month
    * Remove any non-roadmap tasks from that milestone, and put them in the "Tor: unspecified" milestone


Changing Tasks

New tasks / Adding tasks: (everyone)
    * New tasks must have points estimates (N)
    * if the new task is really small (N < 0.5), and you can do it right away:
        * do it now
        * put it in the current month/roadmap/milestone
    * If the new task is essential, put it in the current month/roadmap/milestone
        * remove N points of tasks from that month/roadmap/milestone
        * ask the project manager for help if you can't find any tasks to remove
    * If the new task is important, but it can wait until we have time, tag it with "041-proposed"
    * Otherwise, just put the task in the "Tor: unspecified" milestone


End of the Month

End of the month: (everyone)

If it's the end of the month, and all your tasks are done:
    * nothing happens

If you have finished all your roadmapped tasks for the month:
    * see if other people need help
    * do all your reviews and merges
    * do a task from next month
    * pick one task from "041-proposed", add it to the roadmap and milestone, and do it

If you did not finish all your roadmapped tasks for the month:
    * work out how many points you have left over (N)
    * for this month, and *each month* (M) remaining on the roadmap:
        * remove tasks that are worth N points from that month
        * tag any tasks you still want to do with "041-proposed"
    * ask the project manager to subtract N*(M+1) points from future roadmaps
    * adjust the roadmap months and milestones for the remaining tasks


End of the Milestone

End of the milestone: (project manager)
    * work out how many points are left over (N)
    * for *each milestone* remaining on this roadmap (M):
        * remove N points of tasks from that milestone
    * subtract N*(M+1) points from the points allocated to future roadmaps

End of the milestone: (everyone)
    * for each essential task:
        * if the task is essential before the release, delay the release
        * if the task can wait until after the release, move it to the next milestone
    * remove all non-essential tasks from the milestone and roadmap
        * tag any tasks you still want to do with "042-proposed"


End of the Roadmap

End of the roadmap: (project manager)
    * work out how many points are left over (N)
    * subtract N points from the points allocated to future roadmaps

End of the roadmap: (everyone)
    * move essential tasks to the next roadmap
    * remove all tasks from the roadmap
        * tag any tasks you still want to do with "(next)-roadmap-proposed"



Ideal state

All roadmapped tickets have a points estimate.

The months/milestones/roadmap are under capacity.

All tickets in the milestone are:
* on the roadmap
* allocated to months


Questions:

    * What does the triage person do for milestone assignment with proposed tickets?
      teor says:
        * the bug triage person puts most tickets in "Tor: unspecified"
        * if the ticket doesn't have an estimate, give it an estimate (N points)
        * if the ticket is essential, it goes in the current milestone/month/roadmap,
          and then they remove N points of tickets from the same milestone/month/roadmap.

Nick has some questions:
    * What do we do with tickets that are regression bugs, trivial, etc?
      teor says: essential tickets go in the milestone, and we swap out that many points of tickets
                 very small tickets (N < 0.5) get done right now, and put in the milestone
    * Do we use the pad to accumulate tickets for discussion too?
      teor says: No discussion of tickets at meetings!
                 People manage their own tickets and capacity.
                 If there are issues, the project manager helps adjust tickets.
    * What do we do with tickets that are currently in 0.4.0 and earlier?
      teor says: We dump old tickets in Tor: unspecified.
                 If old tickets are essential, we put them in the next roadmap/milestone, and remove other tickets.
    * What do we do with bugs that people find in current releases?
      teor says: Are they essential? We put them on the roadmap, and remove other tickets.
                 If not, they go in Tor: unspecified.

teor has some questions:
    * What if our estimates are really low? <--- estimates are *never* low
      Suggestion: if we ever have a roadmap where every month is low, we can revise this process.



Existing Processes that Work

Here's what we do right now, and we should keep doing:
* each release is roadmapped at our team meetings
* we put tickets in the release if they're on the roadmap
* bug fixes and features with code go in the latest milestone
  * paid staff can do quick fixes and features up to half a day per week without changing the roadmap
  * volunteers get their contributions reviewed

Problems with Existing Processes

What are the Tor milestones in trac used for?
* tickets that are on the roadmap
* tickets that are urgent (CI, regressions)
* code from volunteers
* things someone would like to do in the release, or would like to put on the roadmap <-- I want to stop using milestones for "Nice to have"

Heres what I want to change:
* Sometimes, people just add tickets to the release
* We currently tag tickets with 040-roadmap-proposed, but that doesn't work, because we don't actually do anything with those tickets
