Source: http://meetbot.debian.net/tor-meeting/2019/tor-meeting.2019-03-05-21.05.log.txt

Original proposal: https://pad.riseup.net/p/network-team-triage-2018


(teor)
  Some background: we put a lot of tickets in milestones, but we don't end up doing them by the time the milestone is over
  I suggested a new proposed tickets process in December 2018, where we tagged tickets and talked about them at the meeting.
  It didn't work: meeting time is precious!


(nickm)
  So my perspective, such as it is: I like the thing where we don't put thing right into 0.x.y-final, but rather add an 0xyz-proposed tag to them...
  but I think we need a clear set of criteria for what can go straight into 0.x.y-final...
  and I hope that we come up with a better procedure for approving 0xy-proposed tickets


On release milestones:

(teor) I would like us to work out what we want to use release milestones for. And then bring that proposal back to the rest of the team.
(catalyst) teor: do you consider release milestones to be independent of release branches?
(teor) In general, the closed tickets in a release milestone on trac have been merged to that release branch (and all future branches)


(nickm) Personally, without thinking too deeply: I would like release milestones to be accurate predictions of what goes out in a given release series. I'd like the features in that branch to be done by the freeze date.
(teor)) I would also like the bugfixes in that branch to be done about a week before the stable release date.



(catalyst) i think they're somewhat tightly coupled, because we generally want changes to go on the release branch for that milestone, so waiting until something's done to decide which release it goes in is difficult unless we change a lot of our process


(nickm) I think we need 2-3 weeks before the stable release date, given that we want to have the fixes in an rc before we declare stable

(nickm) We also have to think about the TB release schedule, which is itself a can of worms


SUGGESTION: on how to add issues to release milestones.

---) (teor) Here's an alternative suggestion: Our largest backlog is tickets that are in new, assigned, needs_information, So let's add a ticket to a release milestone when it is in needs_review.


(teor) nickm: Yes. And on the theory that the set of patches that don't make it in releases is much smaller than the set of proposed tickets that don't make it in releases.
(teor) catalyst: "Most tickets" = anything that has a flexible timeframe. Features, non-urgent bugfixes, etc.


# PROBLEM: what to work on next?

(nickm) I agree with that. But one problem is that it doesn't mesh with our roadmapping and release planning so well.  I often want to find "what should I work on".  When I do that, I usually look at "what's in this release" and "what's assigned to me".
(nickm) I would be okay if there were another place I could look for "what should I work on" ... but 'tor: unspecified' is too big to be that place, of course
(nickm) I could look at the release milestone, plus whatever is in "this-release-proposed" ?

(teor) nickm: in my experience, releases can also be too big to be that place
* catalyst wishes we had a proper backlog with fine-grained ordering of tickets

(nickm) So I usually do the roadmap, but it leads to trouble like right now, 040 needs to happen.  That isn't roadmapped, so it isn't happening.
(nickm) Also, often we have open-ended sponsors like S31.

(teor) gaba: I don't really have a process that helps me identify roadmap, reviews, or high-priority tickets. Let alone other essential things that we haven't roadmapped.

(nickm) I want a summary of what we ought to do for each release, and what we are suggesting that we ought to do



# PROBLEM: tickets with description too broad.

(nickm) We might have a ticket that says "modularize things!" and a hundred child tickets that we _could_ do but they might all be optional.

21:27:25 (catalyst) i think periodic processes have important uses. i'd like them to be as unintrusive as possible
21:27:39 (nickm) I wish there were also a way to look at the tickets in the milestones that are not roadmapped


# PROBLEM: milestone not achievable by their due dates

21:29:17 (teor) Let's try to outline the high-level goals?
21:30:02 (teor) I want the tickets in our milestones/releases/roadmaps to be achievable by their due dates

21:34:35 (nickm) My roadmap problem is that I don't know how to change it.
21:35:07 (nickm) I can change tickets, but I feel I shouldn't change them, since that would change the roadmap
21:36:59 (nickm) I make new tickets, or put new tickets in a milestone, or change which milestone a ticket is in
21:37:24 (nickm) I try not to put new tickets in an existing milestone, though, because that conflicts with the roadmap as I understand it

# PROBLEM: not having a list of tickets that were merged into every release and tickets soon to be merged.

21:35:03 (teor) I want a list of tickets that was merged into every release, and a list of tickets that will soon be merged into every release.

21:41:17 (teor) Like nickm mentioned, there is no kanban for 0.4.0 bugfixes
21:41:31 (teor) Nor is there a kanban for backports, for example
21:42:31 (teor) I am using trac queries for backports, because there are a limited number of backports.

21:43:54 (nickm) I have a weekly item "look at 0.4.0" on my todo list. without it, I would do ~0 040 work
21:44:15 (nickm) as it is, I'm on track to do very little, and get the release out late again


# PROBLEM: too much context switch

21:44:24 (teor) Can we have a shared list of weekly items and suggested times?

21:44:32 (teor) For example:
21:44:39 (teor) 3 days roadmap coding
21:44:43 (teor) 0.5 days reviews
21:45:04 (teor) 0.5 days bugfixes
21:45:23 (teor) 0.5 days admin
21:45:57 (teor) 0.5 days rotations / roles?

21:46:38 (teor) Otherwise, only the people who notice do the things. And that feels terrible.
21:47:06 (nickm) I feel like I spend more time than that on all the things, and yeah.

21:47:27 (teor) Well, yes. I do think our 3 days of roadmap coding might need to be smaller.

21:47:37 (teor) But at least we can work from there and iterate.

21:47:43 (catalyst) it feels to me like we have far too much interrupt-driven stuff
21:50:27 (catalyst) between reviews and capturing stuff that happens in IRC to open tickets, that ends up being a lot of my time
21:51:10 (catalyst) oh, and noticing when CI breaks and helping people troubleshoot that


NEXT MEETING:
- via voice
- (teor) And I want us to come up with goals and actions, because voice meetings are hard to capture and review.
