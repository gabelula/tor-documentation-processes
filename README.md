# README

Gathering and organizing all the softwre development practices at Tor.

## What processes we have in place?

#### Decision making process
<--
. how a 'process' proposal is being made
. discussion and decision
 -->

#### Code Style & Structure

Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/CodingStandards.md
Source: https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/SupportedPlatforms

#### Version Control
<-- TODO -->

#### Testing
<-- standards and style guidelines -->

Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/WritingTests.md

#### Code Review Guidelines
<--
- assignments
- how to review
- definition of done
-->

Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/HowToReview.md

#### Documentation
<-- styleguide -->

#### Continuos integration
<-- how it works -->

### Merge Policy
<--
. contributaion acceptance criteria
. definition of done
. merge requst checklist
-->

#### Releases

##### Release Policy
<-- goals, scope and principles-->

Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/Maintaining.md
Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/ReleasingTor.md
Source: https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/ReleaseGuidelines

##### Release Planning
<-- governance, process designs for implementing relaases å-->

Source: https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/CoreTorReleases
Source:  https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/TicketTriage
Source: https://pad.riseup.net/p/network-team-triage-2018

####  Security Policy

Source: https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/SecurityPolicy

#### Tooling

Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/HelpfulTools.md
Source: https://trac.torproject.org/projects/tor/wiki/org/process/TorOnTrac

#### Contribution

Source: https://gitweb.torproject.org/tor.git/tree/doc/HACKING/GettingStarted.md

#### Code of conduct


## Other developer documentation

- torguts: https://gitweb.torproject.org/user/nickm/torguts.git/tree/

## Discussions during hackweek in January 2019 in Brussels

- [Stable mantainer](https://trac.torproject.org/projects/tor/wiki/org/meetings/2019BrusselsNetworkTeam/Notes/StableMaintainer)
- [Review Best Practices](https://trac.torproject.org/projects/tor/wiki/org/meetings/2019BrusselsNetworkTeam/Notes/ReviewBestPractices)
- [Release Planning](https://trac.torproject.org/projects/tor/wiki/org/meetings/2019BrusselsNetworkTeam/Notes/ReleasePlanning)

## From other projects

- [On contributions](https://github.com/quicwg/base-drafts/blob/master/CONTRIBUTING.md)
- [On roadmaps](https://blog.rust-lang.org/2018/03/12/roadmap.html)
- [Practices](https://the-composition.com/the-origins-of-opera-and-the-future-of-programming-bcdaf8fbe960)
- [OONI software development practices](https://docs.google.com/document/d/1kRsfBObxFKuhbnWIBfY5iahtaVnVXdZwTCTCx6NJEEQ/edit)
- [Mozilla Commit Policy](https://www.mozilla.org/en-US/about/governance/policies/commit/access-policy/)
- [Best practices for computational chemistry](https://github.com/choderalab/software-development)
- [Github Best pracices](https://resources.github.com/videos/github-best-practices/)
- [Gitlab Release Management](https://about.gitlab.com/handbook/engineering/release-management/)
- [Gitlab Contributors Guideline](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md)
